/**
 * CameraMapBuilder.cpp
 *
 * This object generates a map of the robot's surrounding environment
 * using odometry and camera data.
 */
#include "Camera.h"
#include "CameraMapBuilder.h"
#include "LaserMapBuilder.h"
#include "TimeLogger.h"

using namespace boost;
using namespace mrpt::utils;
using namespace std;

CameraMapBuilder::CameraMapBuilder( Camera& camera,
                                    LaserMapBuilder& laserMB,
                                    Robot& robot,
                                    TimeLogger& timeLogger ) :
    running( false ),
    camera( camera ),
    laserMB( laserMB ),
    robot( robot ),
    timeLogger( timeLogger ),
    map( NULL ),
    xMin( 0.0 ),
    xMax( 0.0 ),
    yMin( 0.0 ),
    yMax( 0.0 ),
    res( 0.0 )
{
}

void CameraMapBuilder::start()
{
    mutex::scoped_lock lock( cmdMutex );

    //  Verify a thread isn't already running
    if( running == true )
    {
        throw runtime_error( "Error: Camera map building has already been started." );
    }

    running = true;

    //  Initialize
    int x = 0;
    int y = 0;
    double phi = 0;
    laserMB.getMapInfoAndPose( xMin,
                               xMax,
                               yMin,
                               yMax,
                               res,
                               x,
                               y,
                               phi );

    map = cvCreateImage( cvSize( ( xMax - xMin ) / res + 1,
                                 ( yMax - yMin ) / res + 1 ),
                         IPL_DEPTH_8U,
                         1 );
    cvRectangle( map,
                 cvPoint( 0, 0 ),
                 cvPoint( map->width, map->height ),
                 CV_RGB( 127, 127, 127 ),
                 CV_FILLED );

    //  Start the thread
    thread = boost::thread( &CameraMapBuilder::run,
                            this );
}

void CameraMapBuilder::stop()
{
    mutex::scoped_lock lock( cmdMutex );

    //  Verify a thread is running.
    if( running == false )
    {
        throw runtime_error( "Error: Camera map building hasn't been started." );
    }

    //  Indicate the thread should stop; wait for it.
    running = false;
    thread.join();

    cvReleaseImage( &map );
}

void CameraMapBuilder::run()
{
    while( running == true )
    {
        TimeLogger::ScopedSection tlSec( timeLogger,
                                         "Camera Map Builder" );

        double laserMapX_Min = 0.0;
        double laserMapX_Max = 0.0;
        double laserMapY_Min = 0.0;
        double laserMapY_Max = 0.0;
        double laserMapRes   = 0.0;
        int x = 0;
        int y = 0;
        double phi;
        IplImage* processedData = NULL;
        IplImage* thresholdData = NULL;

        //  Grab data from the laser range finder map
        laserMB.getMapInfoAndPose( laserMapX_Min,
                                   laserMapX_Max,
                                   laserMapY_Min,
                                   laserMapY_Max,
                                   laserMapRes,
                                   x,
                                   y,
                                   phi );

        //  Check to see if we need to resize the map
        if( ( xMin != laserMapX_Min ) ||
            ( xMax != laserMapX_Max ) ||
            ( yMin != laserMapY_Min ) ||
            ( yMax != laserMapY_Max ) )
        {
            resizeMap( laserMapX_Min,
                       laserMapX_Max,
                       laserMapY_Min,
                       laserMapY_Max );
        }

        for( int i = 0; i < camera.numCameras; i++ )
        {
            //  Grab sensor data and format it.
            processedData = cvCreateImage( cvSize( map->width,
                                                   map->height ),
                                           IPL_DEPTH_8U,
                                           1 ); //  Num channels 
            thresholdData = cvCreateImage( cvSize( map->width,
                                                   map->height ),
                                           IPL_DEPTH_8U,
                                           1 ); //  Num channels 

            formatCameraData( processedData,
                              i,
                              x,
                              y,
                              phi );

            //  Add the image.
            mutex::scoped_lock lock( mapMutex );

            //    - Add obstacles
            cvThreshold( processedData,
                         thresholdData,
                         127.0,         //  Threshold
                         32.0,          //  Value   TODO    Use setting
                         CV_THRESH_BINARY );
            cvSub( map,
                   thresholdData,
                   map );

            //    - Add clear spots
            cvThreshold( processedData,
                         thresholdData,
                         127.0 - 1.0,   //  Threshold
                         32.0,          //  Value TODO    Use setting
                         CV_THRESH_BINARY_INV );
            cvAdd( map,
                   thresholdData,
                   map );

            //    - Save robot pose
            robotX = x;
            robotY = y;
            robotPhi = phi;

            cvReleaseImage( &thresholdData );
            cvReleaseImage( &processedData );
        }
    }
}

void CameraMapBuilder::resizeMap( double& newX_Min,
                                  double& newX_Max,
                                  double& newY_Min,
                                  double& newY_Max )
{
    //  Create a new map large enough for the data
    IplImage* newMap = cvCreateImage( cvSize( ( newX_Max - newX_Min ) / res + 1,
                                              ( newY_Max - newY_Min ) / res + 1 ),
                                      IPL_DEPTH_8U,
                                      1 );

    //  Copy the old map data over
    //    - Setup
    CvPoint2D32f src[3];
    CvPoint2D32f dst[3];
    CvMat* mat = cvCreateMat( 2, 3, CV_32FC1 );

    src[0].x = 0;
    src[0].y = 0;
    src[1].x = map->width;
    src[1].y = 0;
    src[2].x = 0;
    src[2].y = map->height;

    //    - Translate
    for( int i = 0; i < 3; i++ )
    {
        dst[i].x = src[i].x + ( ( xMin - newX_Min ) / res );
        dst[i].y = src[i].y + ( ( newY_Max - yMax ) / res );
    }

    //    - Generate the matrix
    cvGetAffineTransform( src,
                          dst,
                          mat );

    //    - Copy the data
    cvWarpAffine( map,
                  newMap,
                  mat,
                  CV_INTER_LINEAR + CV_WARP_FILL_OUTLIERS,
                  cvScalarAll( 127 ) );

    //  Free resources
    cvReleaseMat( &mat );
    cvReleaseImage( &map );

    //  Save the new map and its properties
    map = newMap;

    xMin = newX_Min;
    xMax = newX_Max;
    yMin = newY_Min;
    yMax = newY_Max;
}

void CameraMapBuilder::formatCameraData( IplImage* processedData,
                                         const int& camIndex,
                                         const int& x,
                                         const int& y,
                                         const double& phi )
{
    CvPoint2D32f src[3];
    CvPoint2D32f dst[3];
    CvMat* mat = cvCreateMat( 2, 3, CV_32FC1 );
    IplImage* img = camera.createImage();

    camera.getRectifiedImage( img, camIndex );
    TPoint2D offset = camera.getCameraOffset( camIndex );

    //  Calculate the needed angle for the transformation.
    //usleep( 75000 );
    CPose2D robotPose;
    laserMB.getPose( robotPose );
    //double ang = -phi - ( M_PI / 2.0 );
    double ang = -robotPose.phi() - ( M_PI / 2.0 );

    //  Top Left
    src[0].x = 0;
    src[0].y = 0;

    //  Top Right
    src[1].x = camera.getMeterInPixels();
    src[1].y = 0;

    //  Bottom Left
    src[2].x = 0;
    src[2].y = camera.getMeterInPixels();

    //  Top Left
    dst[0].x = x + ( -offset.x / res ) * cos( ang ) -
                   ( offset.y / res ) * sin( ang );
    dst[0].y = y + ( -offset.x / res ) * sin( ang ) +
                   ( offset.y / res ) * cos( ang );

        //  Top Right
    dst[1].x = x + ( -( offset.x + 1.0 ) / res ) * cos( ang ) -
                   ( offset.y / res ) * sin( ang );
    dst[1].y = y + ( -( offset.x + 1.0 ) / res ) * sin( ang ) +
                   ( offset.y / res ) * cos( ang );

    //  Bottom Left
    dst[2].x = x + ( -offset.x / res ) * cos( ang ) -
                   ( ( offset.y - 1.0 ) / res ) * sin( ang );
    dst[2].y = y + ( -offset.x / res ) * sin( ang ) +
                   ( ( offset.y - 1.0 ) / res ) * cos( ang );

    //  Perform the transformation
    cvGetAffineTransform( src,
                          dst,
                          mat );

    cvWarpAffine( img,
                  processedData,
                  mat,
                  CV_INTER_LINEAR + CV_WARP_FILL_OUTLIERS,
                  cvScalarAll( 127 ) );

    //  Free resources
    cvReleaseMat( &mat );
    cvReleaseImage( &img );
}

void CameraMapBuilder::getMapAndPose( utils::CImage& map,
                                      int& x,
                                      int& y,
                                      double& phi )
{
    mutex::scoped_lock lock( cmdMutex );

    //  Verify we're building a map...
    if( running == false )
    {
        //  Report the error, but don't throw an exception.  In theory,
        //  this function could have blocked on another thread calling the
        //  stop function.  Once we have control of the mutex, map building
        //  would no longer be running.
        cerr << "Error: Map building isn't running (CameraMapBuilder::getMapAndPose)." << endl;
    }
    else
    {
        mutex::scoped_lock lock( mapMutex );

        map.loadFromIplImage( this->map );
        x = robotX;
        y = robotY;
        phi = robotPhi;
    }
}

void CameraMapBuilder::getMap( utils::CImage& map )
{
    mutex::scoped_lock lock( cmdMutex );

    //  Verify we're building a map...
    if( running == false )
    {
        //  Report the error, but don't throw an exception.  In theory,
        //  this function could have blocked on another thread calling the
        //  stop function.  Once we have control of the mutex, map building
        //  would no longer be running.
        cerr << "Error: Map building isn't running (CameraMapBuilder::getMap)." << endl;
    }
    else
    {
        mutex::scoped_lock lock( mapMutex );

        map.loadFromIplImage( this->map );
    }
}

