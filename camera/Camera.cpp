/**
 * Camera.cpp
 *
 * This object provides an interface to a pair of cameras.
 * It handles acquisition, translation, and stiting to provide
 * an edge map of a top down view of the playing field visible
 * to the cameras. 
 */

#include <mrpt/base.h>
#include <mrpt/obs.h>
#include <mrpt/hwdrivers/CGPSInterface.h>
#include <mrpt/utils/exceptions.h>
#include "Camera.h"
#include <cv.h>
#include <highgui.h>

using namespace mrpt;
using namespace mrpt::utils;
using namespace mrpt::slam;
using namespace mrpt::system;
using namespace mrpt::hwdrivers;
using namespace cv;
using namespace std;

const string simFilePath = "./images/test.jpg";

Camera::Camera(int index)
{
	this->index = index; simFilePath= "./images/test.jpg";
	//CImageGrabber_OpenCV cameraSensor(index);
	cameraSensor = new CCameraSensor();
	const string configFile = "./cameraConfig.ini";
  char section[10];
	sprintf(section,"Camera%d",index);
	//TODO do the config stuff right
	//just a one camera case and not using the nice image transformations
  if (mrpt::system::fileExists(configFile)){
		//open it
    CConfigFile iniFile(configFile);
		//load it
    	cameraSensor->loadConfig( iniFile,section);
		try{
			//initialize
			cameraSensor->initialize();
		}catch(std::exception &e){
			simMode();
			return;
		}
		while (cameraSensor->getState() == mrpt::hwdrivers::CGenericSensor::ssInitializing){
			sleep(50);
		}
		if( cameraSensor->getState() == mrpt::hwdrivers::CGenericSensor::ssWorking ){
			simulation = false;
		}else{
			simMode();
		}
  }
	//no config file (atleast not one we're expecting)
  else{
    cout << "Camera " << ""  <<" - no config found" << "" << endl;
		simMode();
	}
}

/**
 * What to do at construction if we're just doing simulation
 */
void Camera::simMode(){
	//tell user
	cout<<"Camera "<<index<<" - simulation mode"<<endl;
	//mark it
	simulation = true;
}

/**
 * destructor
 * need to delete dynamically allocated sensor object
 */
Camera::~Camera(){
	delete cameraSensor;
}
/**
 * returns unmodified image in the form of an openCV matrix
 */
void Camera::getRawImage(Mat& image){
	CObservationImagePtr mrptImage;
	//ask real camera for an image
	if(!simulation){
		mrptImage = (CObservationImagePtr)(cameraSensor->getNextFrame());
	}
	//load in the sim image
	else{
		mrptImage = CObservationImagePtr(new CObservationImage());
		(&mrptImage->image)->loadFromFile(simFilePath);
	}
	//make an openCV friendly object that is pointing at the image data
	image = Mat((IplImage*)((&mrptImage->image)->getAs<IplImage>()), true );
}

/**
	* apply filter to only get features we want (not grass)
	* apply color filters to only get features we want (lines and flags)
	*/
static void grassFilter(Mat& image){
	CvSize captureSize = image.size();
 	Mat splitR       = Mat(captureSize, CV_8UC1);
 	Mat splitG       = Mat(captureSize, CV_8UC1);
 	Mat splitB       = Mat(captureSize, CV_8UC1);
 	Mat intermediate_grassfilter 	= Mat(captureSize, CV_8UC1);
	//Yes, this is the right order
	Mat RGB[] = {splitB, splitG, splitR};

	split( image, RGB);
  // grass = Blue - 1/2 Green.  Thanks, REDRAVEN.
  intermediate_grassfilter= splitB-splitG*0.5;
  // Data between min_color & max_color will clip to white, else black
  threshold( intermediate_grassfilter, image, 75, 255, 0);
	//erode false positives and dilate on disconnected features
  erode ( image, image, Mat());
  dilate( image, image, Mat(), Point(-1,-1), 8 );
}
/**
 * remove barrels from the picture by picking out orange and the area around it
 * then removing those spots from our original picture 
 */
static void barrelFilter(Mat& image){
	CvSize captureSize = image.size();
 	Mat splitR       = Mat(captureSize, CV_8UC1);
 	Mat splitG       = Mat(captureSize, CV_8UC1);
 	Mat splitB       = Mat(captureSize, CV_8UC1);
 	Mat intermediate_barrelfilter 	= Mat(captureSize, CV_8UC1);
	//Yes, this is the right order
	Mat RGB[] = {splitB, splitG, splitR};
	split( image, RGB);
  // orange = red-green
  intermediate_barrelfilter= splitR-splitG;
  // Data between min_color & max_color will clip to white, else black
  threshold( intermediate_barrelfilter, intermediate_barrelfilter, 80, 255, 0);
	//now occlude the white that was on the barrels
	//define a kernel to dilate on
	Mat kernel = getStructuringElement(MORPH_RECT, Size(5,30));
	dilate(intermediate_barrelfilter, intermediate_barrelfilter, kernel, Point(-1,-1),5);
	erode(intermediate_barrelfilter, intermediate_barrelfilter, kernel, Point(-1,-1),3);
	//now apply that to the original image
	subtract(image, 255, image, intermediate_barrelfilter);
}


int main (int argc, char* argv[]){
	Camera* c;
	//error short-circuit
	if(argc == 1){cout<<"specify cameras to open as command line arguments"<<endl; return 0;}
	//iterate over arguments
	for (int i = 2; i<= argc; i++){
		//new camera
		c = new Camera(atoi(argv[i-1]));
		Mat image;
		//go until <esc>
		while(true){
			//raw image
			c->getRawImage(image);
			namedWindow("raw",CV_WINDOW_AUTOSIZE);
			Mat raw = image.clone();
			imshow("raw", raw);
			//after barrel filter
			barrelFilter(image);
			namedWindow("barrel filter",CV_WINDOW_AUTOSIZE);
			Mat barrelFilter = image.clone();
			imshow("barrel filter", barrelFilter);
			//after grass filter
			grassFilter(image);
			namedWindow("grass filter",CV_WINDOW_AUTOSIZE);
			Mat grassFilter = image.clone();
			imshow("grass filter", grassFilter);
			if((char)cvWaitKey(10) == 27) break;
		}
	}
}
