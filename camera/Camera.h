#ifndef CAMERA_H
#define CAMERA_H

#include <mrpt/hwdrivers/CCameraSensor.h>
#include <cv.h>
using namespace cv;
/**
 * This object provides an interface to a pair of cameras.
 * It handles acquisition, translation, and stiting to provide
 * an edge map of a top down view of the playing field visible
 * to the cameras. 
 */
class Camera
{
  private: 
		//corresponds to the index of the config file to read from
		int index;
		//Camera is essentially a wrapper for this, and image transformations
		mrpt::hwdrivers::CCameraSensor* cameraSensor;
		//t/f, whether we're running in sim mode (auto-detects)
		bool simulation;
		//where we're going to look for the config file
		//TODO put file path in config file
		std::string simFilePath;
		/**
 		* What to do at construction if we're just doing simulation
		* No one needs to see this except developer, just makes things a 
		* little neater and potentially easier to modify
 		*/
		void simMode();

  public:
		/**
		 * get an image from the camera
		 * @param image - opencv Mat reference where the image should be written to
		 * NOTE - this creates a copy of the image taken when the camera is queried
		 * 
		 */
		void getRawImage(Mat& image); 
		/**
		 * apply filter to only get features we want (not grass)
		 * @param image - opencv Mat reference, image to perform filtering on
		 * 		midifies the image to a B/W where white represents a line on the ground
		 */
		static void grassFilter(Mat& image);
		/**
 		* remove barrels from the picture by picking out orange and the area around it
 		* then removing those spots from our original picture 
		* @param image - opencv Mat reference, image to perform filtering on
		* 		modifies the image by turning what we perceive as barrels into black spots
 		*/
		static void barrelFilter(Mat& image);

    /**
     * Default contructor. 
		 * @param index - number corresponding to the config file section this camera should read from
		 * 		as written, this corresponds to the V4L index to attempt to open
     */
    Camera( int index);
		/**
		 * Default destructor
		 *
		 */
		~Camera();
};

#endif /* CAMERA_H */

