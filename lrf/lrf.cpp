#include <mrpt/slam.h>
#include <mrpt/base.h>
#include <mrpt/gui.h>
#include <mrpt/utils.h>
#include <mrpt/hwdrivers/CIbeoLuxETH.h>
#include <iostream>

using namespace mrpt;
using namespace mrpt::utils;
using namespace mrpt::poses;
using namespace mrpt::slam;
using namespace mrpt::gui;
using namespace mrpt::hwdrivers;
using namespace std;

/**
A short program to demonstrate the LRF and what it sees.
This is not intended to relate directly to mapping or the robot in anyway.
This will likely be modified in the future to integrate with other sensors.
This was the RANSAC demo picked apart essentially for the graphics part
**/
mrpt::gui::CDisplayWindow3DPtr  win;

int main(int argc, char* argv[]){
	//make the lrf and get a scon
	CIbeoLuxETH lrf(string("INSERT IP ADDRESS HERE"), INSERT PORT HERE);
	lrf.turnOn();
	bool isOutObs, hardwareError;
	CObservation2DRangeScan obs();
	lrf.doProcessSimple(isOutObs, obs, hardwareError);
	//stick the points into the window
	vector_double xs,ys,zs;
	
	
	
	// Show GUI
	// --------------------------
	win = mrpt::gui::CDisplayWindow3DPtr( new mrpt::gui::CDisplayWindow3D("RANSAC: 3D planes", 500,500));

	opengl::COpenGLScenePtr scene = opengl::COpenGLScene::Create();

	scene->insert( opengl::CGridPlaneXY::Create(-20,20,-20,20,0,1) );
	scene->insert( opengl::stock_objects::CornerXYZ() );
	

	//set up the thing that holds the points
	opengl::CPointCloudPtr points = opengl::CPointCloud::Create();
	points->setColor(0,0,1);
	points->setPointSize(3);
	points->enableColorFromZ();
	//shove the ranges of the scan into the pointcloud
	//FIXME - this is obviously not representative of the scan
	points->setAssPoints(obs->scan, obs->scan, obs->scan);

	win->get3DSceneAndLock() = scene;
	win->unlockAccess3DScene();
	win->forceRepaint();


	win->waitForKey();
    
}
