/* +---------------------------------------------------------------------------+
   |                 The Mobile Robot Programming Toolkit (MRPT)               |
   |                                                                           |
   |                          http://www.mrpt.org/                             |
   |                                                                           |
   | Copyright (c) 2005-2013, Individual contributors, see AUTHORS file        |
   | Copyright (c) 2005-2013, MAPIR group, University of Malaga                |
   | Copyright (c) 2012-2013, University of Almeria                            |
   | All rights reserved.                                                      |
   |                                                                           |
   | Redistribution and use in source and binary forms, with or without        |
   | modification, are permitted provided that the following conditions are    |
   | met:                                                                      |
   |    * Redistributions of source code must retain the above copyright       |
   |      notice, this list of conditions and the following disclaimer.        |
   |    * Redistributions in binary form must reproduce the above copyright    |
   |      notice, this list of conditions and the following disclaimer in the  |
   |      documentation and/or other materials provided with the distribution. |
   |    * Neither the name of the copyright holders nor the                    |
   |      names of its contributors may be used to endorse or promote products |
   |      derived from this software without specific prior written permission.|
   |                                                                           |
   | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       |
   | 'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED |
   | TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR|
   | PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE |
   | FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL|
   | DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR|
   |  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)       |
   | HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,       |
   | STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN  |
   | ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE           |
   | POSSIBILITY OF SUCH DAMAGE.                                               |
   +---------------------------------------------------------------------------+ */

#include <mrpt/base.h>
#include <mrpt/obs.h>
#include <mrpt/hwdrivers/CGPSInterface.h>
#include "GPS.h"

using namespace mrpt;
using namespace mrpt::utils;
using namespace mrpt::slam;
using namespace mrpt::system;
using namespace mrpt::hwdrivers;
using namespace std;

//const string SERIAL_NAME;     // Name of the serial port to open

GPS::GPS(){
	bool timeOutCondition = false;//TODO define this in the connection loop for automatic simulation on sensor failure
	SERIAL_NAME = "/dev/ttyUSB0";
	gpsSensor = new CGPSInterface();
	//look for a config file to read from
  if (mrpt::system::fileExists("./CONFIG_gps.ini")){
		//open it
    CConfigFile iniFile("./CONFIG_gps.ini");
		//load it
    gpsSensor->loadConfig( iniFile,"GPS");
	}
	//no config file (atleast not one we're expecting)
  else{
		//use the hardcoded one
		//print it out, just for clarity
    cout << "gps - Using serial port: " << SERIAL_NAME << endl;
		// Set the gps serial port:
		gpsSensor->setSerialPortName(SERIAL_NAME);
	}
	//tell us if the sensor is initialized
	while(!gpsSensor->isGPS_connected()){
		cout<<"[GPS]-awaiting connection"<<endl;
		gpsSensor->doProcess();
		sleep(5000);
		if(timeOutCondition){
			cerr<<"[GPS]-connection timeout: reverting to simulation"<<endl;
			simulation = true;
		}
	}
	simulation=false;
}
GPS::~GPS(){
	delete gpsSensor;
}
CObservationGPSPtr GPS::getCoords(){
	//we don't have a real sensor. Short-circuit with BS
	if(simulation){
		//TODO fill in other fields as needed
		CObservationGPSPtr gpsData(new CObservationGPS());
		gpsData->has_GGA_datum = true;
		gpsData->GGA_datum.latitude_degrees=90.0;
		gpsData->GGA_datum.longitude_degrees=0.0;
		return gpsData;
	}
	//make a list of observations. This is where the gps data will enqueue
	CGenericSensor::TListObservations lstObs;
  CGenericSensor::TListObservations::reverse_iterator itObs;
	//go until we get something
	while (lstObs.empty()){
		cout<<"[GPS] Waiting for data..."<<endl;
		//gps, go do something
		gpsSensor->doProcess();
		//chill for a bit
		mrpt::system::sleep( 10000 );
		//get the observations and empty the queue
		gpsSensor->getObservations( lstObs );
		//otherwise, do stuff with the data
	}
	//we have data at this point
	//grab the last element
	itObs=lstObs.rbegin();
	//this should be a gps observation. If it's not, we have issues
	ASSERT_(itObs->second->GetRuntimeClass()==CLASS_ID(CObservationGPS));
	//get the observation part of the element
	CObservationGPSPtr gpsData=CObservationGPSPtr(itObs->second);
	//copy the value
	//FIXME there's a good chance this copy won't carry over but we'll try anyways
	//if there's a problem with gps observations dissappearing or becoming null, check here
	coordinates=(CObservationGPSPtr)gpsData->duplicateGetSmartPtr();
	//delete all the observations
	lstObs.clear();
	return coordinates;
}

float dist(CObservationGPSPtr p1, CObservationGPSPtr p2){
	float r=6371000;
	double pi = acos(-1);
	if(p1->has_GGA_datum && p2->has_GGA_datum){
		//use GGA datum
		CObservationGPS::TGPSDatum_GGA coord1;
		CObservationGPS::TGPSDatum_GGA coord2;
		//everything in radians
		double lon1 = p1->GGA_datum.longitude_degrees*(pi/180.0);
		double lon2 = p2->GGA_datum.longitude_degrees*(pi/180.0);
		double lat1 = p1->GGA_datum.latitude_degrees*(pi/180.0);
		double lat2 = p2->GGA_datum.latitude_degrees*(pi/180.0);
		double x = (lon2-lon1) * cos((lat1+lat2)/2);
		double y = lat2-lat1;
		return sqrt(x*x+y*y)*r;
	}
	return 0;
}
float bearing(CObservationGPSPtr p1, CObservationGPSPtr p2){
	double pi = acos(-1);
	if(p1->has_GGA_datum && p2->has_GGA_datum){
		//use GGA datum
		CObservationGPS::TGPSDatum_GGA coord1;
		CObservationGPS::TGPSDatum_GGA coord2;
		//everything in radians
		double lon1 = p1->GGA_datum.longitude_degrees*(pi/180.0);
		double lon2 = p2->GGA_datum.longitude_degrees*(pi/180.0);
		double lat1 = p1->GGA_datum.latitude_degrees*(pi/180.0);
		double lat2 = p2->GGA_datum.latitude_degrees*(pi/180.0);
		double y = sin(lon2-lon1)*cos(lat2);
		double x = cos(lat1)*sin(lat2) - sin(lat1)*cos(lat2)*cos(lon2-lon1);
		double bearing= atan2(y,x)+(2*pi);
		if(bearing >= 2*pi){ bearing-=(2*pi);}
		return bearing;
	}
	return 0.0;

}

int main(){
	GPS gypsy;
	CObservationGPSPtr gpsData;
	gpsData = gypsy.getCoords();
	gpsData->dumpToConsole();
}



