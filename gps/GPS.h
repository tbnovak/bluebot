#ifndef GPS_H
#define GPS_H
#include <mrpt/hwdrivers/CGPSInterface.h>

class GPS{
	public:
		//constructor
		GPS();
		//destructor
		~GPS();
		/**grab gps coordinates
		 *@return - an mrpt object containing all the information the GPS can give us
		 */
		mrpt::slam::CObservationGPSPtr getCoords();
		/**
		*helper function 
		*uses pythagoras' theorem on an equirectangular projection which is good enough for nearby points
		* (read "nearby points" as "anywhere we're ever going to tell this robot to go aside from BS sim data)
		*@param p1 - gps info corresponding to one point
		*@param p2 - gps info corresponding to the other point
		* NOTE: only the latitude and longitude in the TGPSDATUM_GGA struct are used
		*@return - distance in meters between the two coordinates
		*/
		static float dist(mrpt::slam::CObservationGPSPtr p1, mrpt::slam::CObservationGPSPtr p2);
		/**
		 * helper function to calculate bearing from one point to the next
		 * @param p1 - start point
		 * @param p2 - destination
		 * @return - initial bearing for shortest great-arc path from p1 toward p2 (in radians (0,2pi)
		 * 		0    -North
		 * 		pi/2 -East
		 * 		pi   -South
		 * 		3pi/2-West
		 * 		
		 */
		//return bearing from p1 to p2 in radians (0,2pi) where N is 0 and increases clockwise
		static float bearing(mrpt::slam::CObservationGPSPtr p1, mrpt::slam::CObservationGPSPtr p2);

	private:
		//GPS is basically a wrapper for this mrpt sensor interface (plus helper stuff related to gps)
		mrpt::hwdrivers::CGPSInterface* gpsSensor;
		std::string SERIAL_NAME;// Name of the serial port to open if we don't use config
		//t/f whether we're using a real sensor or simulation data (autodetect)
		bool simulation;
		//last coordinates received by the GPS (object includes data such as timestamp)
		mrpt::slam::CObservationGPSPtr coordinates;

};
#endif
