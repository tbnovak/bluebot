#include "RazorAHRS.h"
//#include <mrpt/slam/CObservationIMU.h>
#include <iostream>
#include <stdexcept>
#include <cstdio>
#include <iomanip>
using namespace std;
//using namespace mrpt::slam;

enum {accX=0,accY,accZ,magX,magY,magZ,yaw,pitch,roll};
float pose[9];
const string portname = "/dev/ttyACM0";

//function called by the IMU
void onData(const float data[]){
	pose[accX]=data[0];
	pose[accY]=data[1];
	pose[accZ]=data[2];
	pose[magX]=data[3];
	pose[magY]=data[4];
	pose[magZ]=data[5];
	pose[yaw]=data[6];
	pose[pitch]=data[7];
	pose[roll]=data[8];
}
//function called by the IMU
void onError(const string &message){
	cerr<<message<<endl;
}


class IMU
{
	private:
		RazorAHRS *razor;
	public:
		IMU(){
			//RazorAHRS::ACC_MAG_GYR_CALIBRATED
			try{
    		razor = new RazorAHRS(portname, onData, onError, RazorAHRS::ACC_MAG_GYR_RAW);
				//sensor = new RazorAHRS(portname, onData, onError, RazorAHRS::ACC_MAG_GYR_RAW);
			}catch(runtime_error &e){
				cerr<<"Couldn't create tracker"<<endl;
			}
		}
		int /**CObservationIMUPtr**/ getObservation(){
			//CObservationIMUPtr ret = CObservationIMUPtr(new CObservationIMU());
			int ret = 10000;
			return ret;
		}
};

int main(int argc, char* argv[]){
	IMU* imu = new IMU();
	while(true){
		for(int i=0; i<=8; i++){
			cout<<pose[i]<<" ";
		}
	}
	cout<<endl;
	return 0;
}
